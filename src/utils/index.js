const fs = require("fs");

module.exports.subFiles = (dir) => {
  return fs
    .readdirSync(dir, { withFileTypes: true })
    .filter((dirent) => !dirent.isDirectory())
    .map((dirent) => dirent.name);
};

module.exports.subDirectories = (dir) => {
  return fs
    .readdirSync(dir, { withFileTypes: true })
    .filter((dirent) => dirent.isDirectory())
    .map((dirent) => dirent.name);
};

// Min - include | max - include
module.exports.randomWithoutRarity = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

module.exports.random = (images) => {
  const maximum = images
    .map((element) => element.rarity)
    .reduce((sum, current) => sum + current, 0);

  const randomValue = Math.random() * maximum;
  let cur = 0;

  for (const image of images) {
    cur = cur + image.rarity;
    if (cur > randomValue) return image;
  }
};
