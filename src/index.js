const fs = require("fs");
const path = require("path");

const sharp = require("sharp");

const { createCanvas, loadImage } = require("canvas");
const { subDirectories, subFiles, randomWithoutRarity } = require("./utils");

const layersPath = path.join(__dirname, "layers");
const buildPath = path.join(__dirname, "build");
const webpPath = path.join(__dirname, "webp");

async function loadLayers() {
  const result = [];
  const layers = subDirectories(layersPath);

  for (const layer of layers) {
    const files = subFiles(path.join(layersPath, layer));

    const data = files.filter(
      (el) => el.endsWith(".png") || el.endsWith(".jpg")
    );

    const images = await Promise.all(
      data.map(async (el) => ({
        name: el,
        img: await loadImage(path.join(layersPath, layer, el)),
      }))
    );

    result.push({ name: layer, images });
  }

  return result;
}

async function renderImage(name, data) {
  const canvas = createCanvas(3000, 3000);
  const ctx = canvas.getContext("2d");

  for (const el of data) {
    ctx.drawImage(el, 0, 0);
  }

  const imagePath = path.join(buildPath, `${name}.png`);
  fs.writeFileSync(imagePath, canvas.toBuffer("image/png"));

  await sharp(canvas.toBuffer())
    .resize(500)
    .webp()
    .toFile(path.join(webpPath, `${name}.webp`));

  console.log(`${name}: Image generate successful`);
}

const right = [
  "61.png",
  "62.png",
  "63.png",
  "66.png",
  "67.png",
  "68.png",
  "613.png",
  "614.png",
  "615.png",
  "616.png",
  "617.png",
];

function createImage(layers) {
  const result = [];
  const metadata = [];
  let rightArm = null;
  let trunk = null;

  for (const layer of layers) {
    const images = layer.images;
    let randomize = randomWithoutRarity(0, images.length - 1);

    if (layer.name == "04 2. Trunk") {
      trunk = images[randomize].name;
    }

    if (
      layer.name == "09 1. Head" &&
      trunk != "2B1.png" &&
      trunk != "2B2.png" &&
      trunk != "2B3.png"
    ) {
      randomize = images.findIndex((el) => el.name == trunk);
    }

    if (layer.name == "03 6. Right Arm") {
      rightArm = right.findIndex((el) => el == images[randomize].name) != -1;
    }

    if (layer.name == "08 4 . Weapon" && rightArm) {
      continue;
    }

    if (!images[randomize]?.img) {
      console.log(trunk);
    }

    result.push(images[randomize]?.img);
    metadata.push({
      layerName: layer.name,
      imageName: images[randomize].name,
    });
  }
  return { data: result, metadata };
}

async function main() {
  const layers = await loadLayers();

  const data = [];
  for (let i = 1; i < 501; i++) {
    const image = createImage(layers);

    const dup = data.find(
      (el) => JSON.stringify(el) == JSON.stringify(image.metadata)
    );

    if (dup) {
      i--;
      continue;
    }

    await renderImage(`${i}`, image.data);
    data.push(image.metadata);
  }

  fs.writeFileSync("./data.json", JSON.stringify(data, null, 2));
}

main();
